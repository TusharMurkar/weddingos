 $(window).load(function () {
     $('.1-priority').css('display', 'block');
 });

 $(function () {
     console.log("SCROLL FUNCTION ", $);
     // window.scroll menu ite activated    
     $(window).scroll(function () {
         activeMenuItem();
     });



     // function for active menuitem
     var sections = $('section'),
         nav = $('#nav'),
         nav_height = nav.outerHeight();

     function activeMenuItem() {
         var cur_pos = $(window).scrollTop() + 2;
         sections.each(function () {
             var top = $(this).offset().top - nav_height,
                 bottom = top + $(this).outerHeight();

             if (cur_pos >= top && cur_pos <= bottom) {
                 //                 console.log(this);

                 nav.find('ul > li > a').parent().removeClass('active');
                 nav.find('a[href="#' + $(this).attr('id') + '"]').parent().addClass('active');

             } else if (cur_pos == 2) {
                 nav.find('ul > li > a').parent().removeClass('active');
             }
         });
     }

     // smooth-scrolling click menu item
     $(function () {
         $('#navbar > ul > li > a').click(function () {
             if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                 var target = $(this.hash);
                 target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                 if (target.length) {
                     $('html, body').animate({
                         scrollTop: target.offset().top - 100
                     }, 500);
                     return false;
                 }
             }
         });
     });


     // home-slider
     $('.slider').owlCarousel({
         singleItem: true,
         autoPlay: true,
         navigation: true,
         navigationText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
         mouseDrag: false
     });


     // event animation
     $('#event .content .row:first .col:first').each(function () {
         var $this = $(this);
         var myVal = $(this).data("value");

         $this.appear(function () {
             $('#event #first-row .col:first').addClass('animated fadeInLeft');
         });
     });

     $('#event #second-row .col:last').each(function () {
         var $this = $(this);
         var myVal = $(this).data("value");

         $this.appear(function () {
             $('#event #second-row .col:last').addClass('animated fadeInRight');
         });
     });

     $('#event #last-row .col:last').each(function () {
         var $this = $(this);
         var myVal = $(this).data("value");

         $this.appear(function () {
             $('#event #last-row .col:first').addClass('animated fadeInLeft');
         });
     });






     // about Lightbox pluging
     $('.gallery .col a').nivoLightbox();


     // video-bg Lightbox pluging
     $('.video-bg a').nivoLightbox();






 });

 $(document).ready(function () {






     var codeurl = '';
     var i = 0;






     $(window).load(function () {


         var url = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
         console.log(url);
         var urlarr = url.split("-");
         console.log(urlarr);
         codeurl = urlarr[1];

         if (codeurl != undefined) {
             console.log(codeurl);
             $(".popupgobtn").click();
         }




     });



     $(".popupgobtn").click(function () {
         console.log("POPUP BTN ACCEPTED");
         var flip = true;
         i++;
         if (codeurl == undefined) {
             codeurl = $('#invitecode').val();
             console.log("happening");
         }
         console.log(codeurl);
         var codenumber = codeurl.substring(codeurl.length - 4, codeurl.length);
         var name = codeurl.substring(0, codeurl.length - 4);
         var lastletter = name.substring(name.length - 1, name.length);
         var fly = false;
         if (lastletter == '7') {
             name = name.substr(0, name.length - 1);
             fly = true;
         };
         name = capitalizeFirstLetter(name);

         console.log(codenumber);





         $('.Wedding-priority').show();


         if (flip) {
             console.log(flip);
             console.log("inside flip");
             document.getElementsByClassName("popup")[0].style.transform = "rotateY(0deg)";


             for (var i = 0; i < $('.event-div').length; i++) {
                 if ($('.event-div').eq(i).css('display') == 'flex') {
                     $('.event-div').eq(i).addClass('flex-div');
                 }

             }

             for (var i = 0; i < $('.flex-div').length; i++) {

                 if (i % 2 != 0) {
                     $('.flex-div').eq(i).addClass('odd-event');


                 }
             }
             setTimeout(function () {
                 $(".popupoverlay").addClass("animated fadeOutUp");
                 $(".body-div").addClass("disp-block animated slideInUp");
                 var center = $(window).width() / 2;
                 var btnFromLeft = $(".yes-btn").offset().left + $(".yes-btn").width();
                 var distCtoBtn = center - btnFromLeft;

                 console.log(btnFromLeft);
                 console.log(distCtoBtn);

                 var perpixchange = 90 / distCtoBtn;

                 /*    $(document).on("mousemove", function (event) {
                     var dist = event.pageX - center;
                     if (Math.abs(dist) <= distCtoBtn) {
                         var angle = dist * perpixchange;
                         angle = 90 + angle;
                         $('.smily-mouth').css('transform', 'rotateX(' + angle + 'deg)');
                     } else {
                         if (dist < 0) {
                             $('.smily-mouth').css('transform', 'rotateX(0deg)');
                         } else {
                             $('.smily-mouth').css('transform', 'rotateX(180deg)');
                         };
                     }
                 });
 */



                 /* OPEN RSVP HEART BUTTON ON SCROLL */
                 var heartbtnopened = false;


                 /*    $(window).scroll(function () {

                        if (!heartbtnopened) {
                            if ($(window).scrollTop() > ($('.rsvp-h-btn').offset().top - 400)) {
                                heartbtnopened = true;
                                $('.rsvp-h-btn').removeClass('heart-beat');
                                $('.rsvp-h-yes').addClass('rsvp-h-yes-pos');
                                $('.rsvp-h-no').addClass('rsvp-h-no-pos');
                            }
                        }
                    }); */

                 /* WHEN CLICKED ON HEART YES-NO BUTTON */
                 /*    $('.h-btn').click(function () {
                     console.log("heart btn click");
                     $('.rsvp-h-btn').css('transform', 'scale(0)');
                     setTimeout(function () {

                         $('.h-btn').hide();
                         $('.h-message').show();
                         $('.rsvp-h-btn').css('transform', 'scale(1)');
                     }, 1000);
                 });

                 $('.rsvp-h-yes').click(function () {
                     console.log("yes btn click");
                     counter(1);
                 });

                 $('.rsvp-h-no').click(function () {
                     console.log("no btn click");
                     counter(0);
                 });
 */
                 function counter(value) {
                     console.log(value);

                     $.ajax({
                         type: 'get',
                         url: 'rsvp.php',
                         data: {
                             code: codeurl,
                             value: value
                         },
                         success: function (data) {
                             console.log(data);
                         }
                     });
                 }




                 var photostr = '';
                 for (var q = 0; q < photos.length; q++) {
                     console.log("gallery");
                     photostr = photostr + '<div class="photo-div col col-xs-6 col-sm-3 item" data-groups=\'["' + photos[q].group + '"]\'><a onclick="onClick(this)" data-lightbox-gallery="gallery1"><div class="img-caption">' + photos[q].caption + '</div><div class="overlay"></div><img src="images/home/gallery/' + photos[q].src + '" alt="Gallery image" class="img img-responsive" ></a></div>';
                 };
                 $('#shuffle-wrapper').html(photostr);


                 setTimeout(function () {
                     //SET PHOTO SLIDER 

                     //GALLERY INITIALIZATION

                     // shuffle
                     var $main = $('#shuffle-wrapper');
                     $main.shuffle({
                         itemSelector: '.item' // the selector for the items in the grid
                     });




                 }, 0);

                 /* reshuffle when user clicks a filter item */
                 $('#filter a').click(function (e) {
                     e.preventDefault();

                     // set active class
                     $('#filter a').removeClass('active');
                     $(this).addClass('active');

                     // get group name from clicked item
                     var groupName = $(this).attr('data-group');

                     // reshuffle grid
                     $('#shuffle-wrapper').shuffle('shuffle', groupName);
                 });

             }, 1500);














         }
     });

     function capitalizeFirstLetter(string) {
         return string.charAt(0).toUpperCase() + string.slice(1);
     };






     // set the google map
     function map1(latitude, longitude) {
         console.log("setting map");
         var myLatLng = new google.maps.LatLng(latitude, longitude);
         var mapProp = {
             center: myLatLng,
             zoom: 18,
             scrollwheel: false,
             mapTypeId: google.maps.MapTypeId.ROAD
         };
         var map = new google.maps.Map(document.getElementById("googleMap-1"), mapProp);

         var marker = new google.maps.Marker({
             position: myLatLng,
             icon: 'images/home/marker.png'
         });

         marker.setMap(map);
         map.set('styles', [{
             "featureType": "water",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#d85665"
             }, {
                 "lightness": 17
             }]
         }, {
             "featureType": "landscape",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#f1dee2"
             }, {
                 "lightness": 20
             }]
         }, {
             "featureType": "road.highway",
             "elementType": "geometry.fill",
             "stylers": [{
                 "color": "#fff"
             }, {
                 "lightness": 17
             }]
         }, {
             "featureType": "road.highway",
             "elementType": "geometry.stroke",
             "stylers": [{
                 "color": "#fff"
             }, {
                 "lightness": 29
             }, {
                 "weight": 0.2
             }]
         }, {
             "featureType": "road.arterial",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#ffffff"
             }, {
                 "lightness": 18
             }]
         }, {
             "featureType": "road.local",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#d83547"
             }, {
                 "lightness": 16
             }]
         }, {
             "featureType": "poi",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#f5f5f5"
             }, {
                 "lightness": 21
             }]
         }, {
             "featureType": "poi.park",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#dedede"
             }, {
                 "lightness": 21
             }]
         }, {
             "elementType": "labels.text.stroke",
             "stylers": [{
                 "visibility": "on"
             }, {
                 "color": "#ffffff"
             }, {
                 "lightness": 16
             }]
         }, {
             "elementType": "labels.text.fill",
             "stylers": [{
                 "saturation": 36
             }, {
                 "color": "#333333"
             }, {
                 "lightness": 40
             }]
         }, {
             "elementType": "labels.icon",
             "stylers": [{
                 "visibility": "off"
             }]
         }, {
             "featureType": "transit",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#f2f2f2"
             }, {
                 "lightness": 19
             }]
         }, {
             "featureType": "administrative",
             "elementType": "geometry.fill",
             "stylers": [{
                 "color": "#fefefe"
             }, {
                 "lightness": 20
             }]
         }, {
             "featureType": "administrative",
             "elementType": "geometry.stroke",
             "stylers": [{
                 "color": "#fefefe"
             }, {
                 "lightness": 17
             }, {
                 "weight": 1.2
             }]
         }]);
     };

 });